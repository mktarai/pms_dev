'use strict'

angular.module('pmsApp')
  .controller('PatientCreateController', ['$scope', '$uibModal', 'PatientFactory', 'CountryFactory', function ($scope, $uibModal, PatientFactory, CountryFactory) {
     $scope.isSubmitted = false;
     $scope.message = '';
     $scope.nationality = 'No Country has been selected.'

     $scope.patient = {};
     $scope.modal = {
        header: '',
        message: '',
        type: ''
     };

     $scope.countryList = [];
     $scope.genderList = [{
            key: 'M',
            value: 'Male'
        }, {
            key: 'F',
            value: 'Female'
        }
     ];

     $scope.maritalList = [{
             key: 'M',
             value: 'Married'
         }, {
             key: 'S',
             value: 'Single'
         }
     ];

     $scope.init = function() {
        $scope.isSubmitted = false;
        $scope.patient = PatientFactory.getObj();
        CountryFactory.findAll().then(function(response) {
            $scope.countryList = response;
        });
     }

     $scope.init();

        $scope.alert = function (size) {
            var modalInstance = $uibModal.open({
                animation: true,
                templateUrl: 'alertModalContent.html',
                controller: 'ModalAlertController',
                size: size,
                resolve: {
                    alert: function() {
                        return {
                            header: $scope.modal.header,
                            message: $scope.modal.message,
                            type: $scope.modal.type
                        };
                    }
                }
            });

            modalInstance.result.then(function (response) {

            }, function () {

            });
        };

     $scope.evalNationality = function(country_id) {
         var result = $scope.countryList.filter(function(element, index, array) {
             return (element.id == country_id);
         });
         if(result.length > 0) {
             return result[0].nationality;
         } else {
             return '';
         }
     };

     $scope.add = function() {
        $scope.isSubmitted = true;
        if($scope.FormPatient.$valid) {
            PatientFactory.create(angular.copy($scope.patient)).then(function(response) {
                  $scope.modal = {
                      header: 'Success',
                      message: response.message,
                      type: 'alert-success'
                   };
                  $scope.alert('sm');
                  $scope.reset();
            }, function(response) {
                 $scope.modal = {
                       header: 'Error',
                       message: response.data.message,
                       type: 'alert-danger'
                 };
                 $scope.alert('sm');
            });
        }
     };

     $scope.reset = function() {
        $scope.FormPatient.$setPristine(true);
        $scope.init();
     };
    }]);