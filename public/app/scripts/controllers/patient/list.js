'use strict'

angular.module('pmsApp')
    .controller('PatientListController', ['$scope', 'PatientFactory', '$uibModal', 'DTOptionsBuilder', 'DTColumnBuilder', function($scope, PatientFactory, $uibModal, DTOptionsBuilder, DTColumnBuilder) {
        $scope.loading = false;
        $scope.isAllSelected = false;

        /*$scope.vm = {};*/
        /*$scope.ids = {};*/
        $scope.criteria = {
            name: '',
            email: '',
            referredBy: '',
            contact: '',
            matchAll: false
        };
        
        $scope.patientList = [];

        $scope.confirm = function (size) {
            var modalInstance = $uibModal.open({
                animation: true,
                templateUrl: 'confirmModalContent.html',
                controller: 'ModalConfirmationController',
                size: size
            });

            modalInstance.result.then(function (response) {
                if(response) {
                    $scope.delete();
                }
            }, function () {

            });
        };

        $scope.loadPatients = function() {
            $scope.patientList = [];
            PatientFactory.findAll().then(function(response) {
                for(var i = 0; i < response.length; i++) {
                    $scope.patientList.push({
                        id: response[i].id,
                        isChecked: false,
                        name: response[i].firstName + ' ' + response[i].lastName,
                        dateOfBirth: (response[i].dateOfBirth) ? moment(response[i].dateOfBirth).format("DD/MM/YYYY") : '',
                        email: response[i].email,
                        cellPhone: response[i].cellPhone,
                        referredBy: response[i].referredBy
                    });
                }
                $scope.loading = false;
            }, function(response) {

            });
        };

        $scope.loadPatients();

        /*function selectHtml(data, type, full, meta) {
            var chkHtml = '<input type="checkbox" id="chk_' + data.id + '" on-click="alert(\'hello\');">';
            return chkHtml;
        }

        $scope.vm.dtColumns = [
            DTColumnBuilder.newColumn('id').withTitle('ID').notVisible(),
            DTColumnBuilder.newColumn(null).withTitle('<input type="checkbox" ng-model="isAllSelected">').withClass('text-center').notSortable().renderWith(selectHtml),
            DTColumnBuilder.newColumn('name').withTitle('Name'),
            DTColumnBuilder.newColumn('dateOfBirth').withTitle('Date Of Birth'),
            DTColumnBuilder.newColumn('email').withTitle('Email'),
            DTColumnBuilder.newColumn('cellPhone').withTitle('Cell Phone'),
            DTColumnBuilder.newColumn('referredBy').withTitle('Referred By'),
        ];
        $scope.vm.dtOptions = DTOptionsBuilder.fromFnPromise(function() {
            return PatientFactory.loadPatientGrid();
        }).withPaginationType('full_numbers');*/

        $scope.enableDeleteButton = function() {
            var result = $scope.patientList.some(function(element, index, array) {
                return (element.isChecked);
            });

            return result;
        };

        $scope.selectAll = function(){
            for(var i=0;i<$scope.patientList.length;i++)
            {
                $scope.patientList[i].isChecked = $scope.isAllSelected;
            }
        };

        $scope.evalSelectAll = function(){
            var result = $scope.patientList.every(function(element,index,array){
                return (element.isChecked);
            });
            $scope.isAllSelected = result;
        };

        $scope.delete = function() {
            var patientIds = [];

            for(var i = 0; i < $scope.patientList.length; i++) {
                if($scope.patientList[i].isChecked) {
                    patientIds.push($scope.patientList[i].id);
                }
            }

            PatientFactory.delete(patientIds).then(function(response) {
                $scope.loadPatients();
            }, function(response) {

            });
        };

        $scope.searchPatients = function() {
            $scope.patientList = [];
            console.log(JSON.stringify($scope.criteria));
            PatientFactory.loadPatientGrid($scope.criteria).then(function(response) {
                for(var i = 0; i < response.length; i++) {
                    $scope.patientList.push({
                        id: response[i].id,
                        isChecked: false,
                        name: response[i].firstName + ' ' + response[i].lastName,
                        dateOfBirth: moment(response[i].dateOfBirth).format("DD/MM/YYYY"),
                        email: response[i].email,
                        cellPhone: response[i].cellPhone,
                        referredBy: response[i].referredBy
                    });
                }
                $scope.loading = false;
            }, function(response) {

            });
        };

        $scope.resetFilters = function() {
            $scope.criteria = {
                name: '',
                email: '',
                referredBy: '',
                contact: '',
                matchAll: false
            };
        };

    }]);
