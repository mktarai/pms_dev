'use strict'

angular.module('pmsApp')
    .controller('PatientDetailController', ['$scope', '$stateParams', '$uibModal', '$state', 'PatientFactory', 'CommonFactory', function($scope, $stateParams, $uibModal, $state, PatientFactory, CommonFactory) {
        $scope.message = '';
        $scope.isEdit = false;
        $scope.isSubmitted = false;
        $scope.isDeleteSuccess = false;
        $scope.isUpdateSuccess = false;

        $scope.patient = {};
        $scope.temp = {
            patient: {}
        };
        $scope.modal = {
                header: '',
                message: '',
                type: ''
             };

        $scope.countryList = [];
        $scope.genderList = CommonFactory.findAllGenderList();
        $scope.maritalList = CommonFactory.findAllMaritalList();

        $scope.maritalList = [{
                 key: 'M',
                 value: 'Married'
             }, {
                 key: 'S',
                 value: 'Single'
             }
        ];

        if($stateParams.id) {
            PatientFactory.loadPatientForm($stateParams.id).then(function(response) {
                if(response) {
                    $scope.patient = PatientFactory.setObj(angular.fromJson(response.patientInfo));
                    $scope.temp.patient = angular.copy($scope.patient);
                    $scope.countryList = angular.fromJson(response.countryList);
                    var result = $scope.countryList.filter(function(element, index, array) {
                        return (element.id == $scope.patient.country);
                    });
                    console.log(angular.toJson(result));
                    if(result.length > 0) {
                        $scope.patient.country =  {id: result[0].id, name: result[0].name};
                    } else {
                        $scope.patient.country =  {id: '', name: 'Select...'};
                    }
                    console.log(angular.toJson($scope.patient.country));

                }
            }, function(response) {

            });
        }

        $scope.confirm = function (size, patient_id) {
            var modalInstance = $uibModal.open({
                animation: true,
                templateUrl: 'confirmModalContent.html',
                controller: 'ModalConfirmationController',
                size: size
            });

            modalInstance.result.then(function (response) {
                if(response) {
                    $scope.delete(patient_id);
                }
            }, function () {

            });
        };

        $scope.alert = function (size) {
                    var modalInstance = $uibModal.open({
                        animation: true,
                        templateUrl: 'alertModalContent.html',
                        controller: 'ModalAlertController',
                        size: size,
                        resolve: {
                            alert: function() {
                                return {
                                    header: $scope.modal.header,
                                    message: $scope.modal.message,
                                    type: $scope.modal.type
                                };
                            }
                        }
                    });

                    modalInstance.result.then(function (response) {
                    if($scope.isUpdateSuccess)
                    {
                        $scope.isEdit = false;
                    }else if($scope.isDeleteSuccess){
                    $state.go('patient-list');
                    }
                    }, function () {

                    });
                };

        $scope.evalNationality = function(country_id) {
             var result = $scope.countryList.filter(function(element, index, array) {
                 return (element.id == country_id);
             });
             if(result.length > 0) {
                 return result[0].nationality;
             } else {
                 return '';
             }
        };

        $scope.evalCountry = function(country_id) {
             var result = $scope.countryList.filter(function(element, index, array) {
                 return (element.id == country_id);
             });
             if(result.length > 0) {
                 return result[0].name;
             } else {
                 return '';
             }
        };

        $scope.update = function() {
             $scope.isSubmitted = true;
             if($scope.FormPatient.$valid) {
                 PatientFactory.update(angular.copy($scope.patient)).then(function(response) {
                 $scope.isUpdateSuccess = true;
                 $scope.isDeleteSuccess = false;
                      $scope.modal = {
                                            header: 'Success',
                                            message: response.message,
                                            type: 'alert-success'
                                         };
                                        $scope.alert('sm');

                 }, function(response) {
                      $scope.isUpdateSuccess = false;
                      $scope.isDeleteSuccess = false;
                      $scope.modal = {
                                             header: 'Error',
                                             message: response.data.message,
                                             type: 'alert-danger'
                                       };
                                       $scope.alert('sm');
                 });
             }
        };

        $scope.delete = function(patient_id) {
            var patientIds = [patient_id];
            PatientFactory.delete(patientIds).then(function(response) {
            $scope.isUpdateSuccess = false;
            $scope.isDeleteSuccess = true;
            $scope.modal = {
                            header: 'Success',
                            message: response.message,
                            type: 'alert-success'
                         };
                        $scope.alert('sm');
            }, function(response) {
            $scope.isUpdateSuccess = false;
                                              $scope.isDeleteSuccess = false;
            $scope.modal = {
                           header: 'Error',
                           message: response.data.message,
                           type: 'alert-danger'
                        };
                         $scope.alert('sm');
                    });
                };

        $scope.cancel = function(){
        $scope.patient = angular.copy($scope.temp.patient);
        $scope.isEdit = false;
    };

    }]);