'use strict'

angular.module('pmsApp')
    .controller('AppointmentCreateController', ['$scope', '$stateParams', '$uibModal', 'AppointmentFactory', function($scope, $stateParams, $uibModal, AppointmentFactory) {

        $scope.isEdit = false;

        $scope.patient = {
            id: $stateParams.id,
            name: '',
            dateOfBirth: '',
            referredBy: '',
            newAppointment: {
                bookDate: new Date(),
                bookTime: new Date()
            },
            appointmentHistoryList: []
        };

        $scope.tp = {
            minDate: new Date()
        };

        $scope.temp = {
            id: '',
            bookDate: '',
            bookTime: ''
        };

        $scope.startDate;
        $scope.currentDate = new Date();

        $scope.kc_OnChange = function(e) {
            console.log(this);
            console.log(this.value());
            console.log(moment(this.value()).isSame(moment(), 'day'))
            if(moment(this.value()).isSame(moment(), 'day')) {
                $scope.tp.minDate = new Date();
            } else {
                $scope.tp.minDate = this.value();
            }
            console.log($scope.tp.minDate);
            $scope.$apply();
        };

        $scope.filterDates = function(date) {
            var d1 = new Date(moment(date).format("YYYY"), moment(date).format("MM"), moment(date).format("DD"));
            var d2 = new Date(moment().format("YYYY"), moment().format("MM"), moment().format("DD"));

            if(d1 != 'Invalid date') {
                if (d1 >= d2) {
                    return false;
                } else {
                    return true;
                }
            }
        };

        $scope.alert = function (size) {
            var modalInstance = $uibModal.open({
                animation: true,
                templateUrl: 'alertModalContent.html',
                controller: 'ModalAlertController',
                size: size,
                resolve: {
                    alert: function() {
                        return {
                            header: $scope.modal.header,
                            message: $scope.modal.message,
                            type: $scope.modal.type
                        };
                    }
                }
            });

            modalInstance.result.then(function (response) {

            }, function () {

            });
        };

        $scope.confirm = function (size, appointment_id) {
            var modalInstance = $uibModal.open({
                animation: true,
                templateUrl: 'confirmModalContent.html',
                controller: 'ModalConfirmationController',
                size: size
            });

            modalInstance.result.then(function (response) {
                if(response) {
                    $scope.delete(appointment_id);
                }
            }, function () {

            });
        };

        $scope.formatAppointmentDate = function() {
            if($scope.patient.newAppointment) {
                return moment($scope.patient.newAppointment.bookDate).format('DD/MM/YYYY');
            } else {
                return '';
            }
        };

        var indexForUpdate = '';
        var item = '';
        $scope.showAppointmentDetail = function(index){
            indexForUpdate = index;
            $scope.isEdit = true;
            item = $scope.patient.appointmentHistoryList[index];
            $scope.patient.newAppointment.bookDate = new Date(item.dateTime);
            $scope.patient.newAppointment.bookTime = item.dateTime;
            $scope.temp.bookTime = moment(item.dateTime).format('hh:mm a');
        };

        $scope.cancel = function() {
            $scope.isEdit = false;
            $scope.patient.newAppointment.bookDate = '';
            $scope.patient.newAppointment.bookTime = '';
            $scope.temp.bookTime = '';
        };

        $scope.delete = function(appointment_id){
            AppointmentFactory.delete(appointment_id).then(function(response){
            $scope.patient.appointmentHistoryList.pop({
               id: appointment_id,
           });

            $scope.modal = {
                    header: 'Success',
                    message: response.message,
                    type: 'alert-success'
            };
            $scope.alert('sm');
            }, function(response) {
                $scope.modal = {
                   header: 'Error',
                   message: response.data.message,
                   type: 'alert-danger'
                };
                $scope.alert('sm');
            });
        };


        $scope.update = function(){
            var appointment_id = item.id;
            $scope.temp.id = appointment_id;

            $scope.temp.bookDate = moment($scope.patient.newAppointment.bookDate).format('DD/MM/YYYY');
            AppointmentFactory.update($stateParams.id, $scope.temp).then(function(response){

                //Remove old date from Appointment History
                $scope.patient.appointmentHistoryList.pop({
                    id: response.appointment.id,
                    dateTime: response.appointment.appointmentDate
                });

                //Add update date from Appointment History
                $scope.patient.appointmentHistoryList.push({
                    id: $scope.temp.id,
                    dateTime: $scope.temp.bookDate+' at '+$scope.temp.bookTime
                });

                $scope.modal = {
                    header: 'Success',
                    message: response.message,
                    type: 'alert-success'
                };
                $scope.alert('sm');
            }, function(response){
                $scope.modal = {
                      header: 'Error',
                      message: response.data.message,
                      type: 'alert-danger'
                };
               $scope.alert('sm');
            });
        }

        $scope.add = function(){
            $scope.temp.bookDate = moment($scope.patient.newAppointment.bookDate).format('DD/MM/YYYY');
            AppointmentFactory.create($stateParams.id, $scope.temp).then(function(response){
                $scope.patient.appointmentHistoryList.push({
                    id: response.appointment.id,
                    dateTime: response.appointment.appointmentDate
                });

                $scope.modal = {
                      header: 'Success',
                      message: response.message,
                      type: 'alert-success'
                   };
                  $scope.alert('sm');
            }, function(response){
                $scope.modal = {
                      header: 'Error',
                      message: response.data.message,
                      type: 'alert-danger'
                };
               $scope.alert('sm');
            });
        };

        AppointmentFactory.loadPatientAppointmentForm($stateParams.id).then(function(response) {
            if(response) {
                if(response.appointmentList.length > 0) {
                    $scope.patient.name = response.appointmentList[0].patient.firstName + ' ' + response.appointmentList[0].patient.lastName;
                    $scope.patient.dateOfBirth = moment(response.appointmentList[0].patient.dateOfBirth).format('DD/MM/YYYY');
                    $scope.patient.referredBy = response.appointmentList[0].patient.referredBy;
                } else {
                    $scope.patient.name = response.patient.firstName + ' ' + response.patient.lastName;
                    $scope.patient.dateOfBirth = response.patient.dateOfBirth ? moment(response.patient.dateOfBirth).format('DD/MM/YYYY') : '';
                    $scope.patient.referredBy = response.patient.referredBy;
                }

                response.appointmentList.forEach(function(element, index, array) {
                    $scope.patient.appointmentHistoryList.push({
                        id: element.id,
                        dateTime: element.appointmentDate
                    });
                });
            }
        }, function(response) {


        });

    }]);