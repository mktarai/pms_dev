'use strict'

angular.module('pmsApp')
    .controller('ModalAlertController', ['$scope', '$uibModalInstance', 'alert', function($scope, $uibModalInstance, alert) {
          $scope.alert = {
            header: alert.header,
            message: alert.message,
            type: alert.type
          };

          $scope.ok = function () {
            $uibModalInstance.close(true);
          };
    }]);
