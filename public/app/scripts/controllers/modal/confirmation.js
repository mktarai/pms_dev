'use strict'

angular.module('pmsApp')
    .controller('ModalConfirmationController', ['$scope', '$uibModalInstance', function($scope, $uibModalInstance) {
          $scope.ok = function () {
            $uibModalInstance.close(true);
          };

          $scope.cancel = function () {
            $uibModalInstance.dismiss('cancel');
          };
    }]);
