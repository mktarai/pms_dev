'use strict';

/**
 * @ngdoc overview
 * @name pmsAppApp
 * @description
 * # pmsAppApp
 *
 * Main module of the application.
 */
angular
  .module('pmsApp', [
    'ngAnimate',
    'ngCookies',
    'ngResource',
    'ngSanitize',
    'ngTouch',
    'ngMaterial',
    'ui.bootstrap',
    'ui.router',
    'restangular',
    'datatables',
    'kendo.directives'
  ])
  .config(['$stateProvider', '$urlRouterProvider', function ($stateProvider, $urlRouterProvider) {

    // For any unmatched url, redirect to /home
    $urlRouterProvider.otherwise("/");
     // Now set up the states
    $stateProvider
        .state('patient-list', {
          url: '/',
          templateUrl: 'views/home/patient/list.html',
          controller: 'PatientListController'
        })
        .state('patient-detail', {
          url: '/detail/{id:int}',
          templateUrl: 'views/home/patient/detail.html',
          controller: 'PatientDetailController'
        })
        .state('patient-create', {
          url: '/create',
          templateUrl: 'views/home/patient/create.html',
          controller: 'PatientCreateController'
        })
        .state('patient-update', {
          url: '/update/{id:int}',
          templateUrl: 'views/home/patient/update.html',
          controller: 'PatientUpdateController'
        })
        .state('appointment-create', {
            url: '/appointment/{id:int}',
            templateUrl: 'views/home/appointment/create.html',
            controller: 'AppointmentCreateController'
        });

  }]);
