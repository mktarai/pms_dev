'use strict'

angular.module('pmsApp')
    .filter('NewLineFilter', ['$sce', function($sce) {
        return function(input) {
            if(input) {
                input = input.replace(/(?:\r\n|\r|\n)/g, '<br />');
                return $sce.trustAsHtml(input);
            } else {
                return '';
            }
        };
    }]);