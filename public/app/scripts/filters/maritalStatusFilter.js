'use strict'

angular.module('pmsApp')
    .filter('MaritalStatusFilter', [function() {
        return function(input) {
            if(input.key) {
                return input.name;
            } else {
                return '';
            }

        };
    }]);