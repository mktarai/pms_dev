'use strict'

angular.module('pmsApp')
    .filter('GenderFilter', [function() {
        return function(input) {
            var name;
            switch(input) {
                case 'M':
                    name = 'Male';
                    break;
                case 'F':
                    name = 'Female';
                    break;
                default:
                    name = '';
            }
            return name;
        };
    }]);