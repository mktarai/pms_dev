'use strict'

angular.module('pmsApp')
    .directive('checkDate', [function() {
        return {
            restrict: 'A',
            require: 'ngModel',
            link: function($scope, $element, $attrs, ngModel) {
                ngModel.$validators.checkDate = function(value) {
                if(value){
                    console.log(value);
                    console.log(moment(value, ["DD/MM/YYYY"], true).isValid());
                    return moment(value, ["DD/MM/YYYY"], true).isValid();
                }


                };

                $scope.$watch($attrs.ngModel, function(value) {
                    ngModel.$validate();
                });
            }
        }
    }]);