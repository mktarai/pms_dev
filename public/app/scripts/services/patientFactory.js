'use strict'

angular.module('pmsApp')
  .factory('PatientFactory', ['Restangular', 'CommonFactory', function(Restangular, CommonFactory) {
    var patientFactory = {};


    var rootUrl = Restangular.all('pms');

    patientFactory.getObj = function() {
      var obj = {
          id: "",
          firstName: "",
          middleName: "",
          lastName: "",
          dateOfBirth: "",
          gender: {
            key: '',
            value: 'Select...'
          },
          maritalStatus: {
             key: '',
             value: 'Select...'
          },
          email: "",
          address: "",
          city: "",
          country: {
             id: '',
             name: 'Select...'
          },
          cellPhone: "",
          alternatePhone: "",
          homePhone: "",
          referredBy: "",
          additionalNotes: ""
      };
      return obj;
    };

    patientFactory.setObj = function(obj_patient) {
      var obj = {
        id: obj_patient.id,
        firstName: obj_patient.firstName,
        middleName: obj_patient.middleName,
        lastName: obj_patient.lastName,
        dateOfBirth: (obj_patient.dateOfBirth) ? moment(obj_patient.dateOfBirth).format("DD/MM/YYYY") : '',
        gender: CommonFactory.findGenderByKey(obj_patient.gender),
        maritalStatus: CommonFactory.findMaritalByKey(obj_patient.maritalStatus),
        email: obj_patient.email,
        address: obj_patient.address,
        city: obj_patient.city,
        country: (obj_patient.country) ? '' + obj_patient.country.id : '',
        cellPhone: obj_patient.cellPhone,
        alternatePhone: obj_patient.alternatePhone,
        homePhone: obj_patient.homePhone,
        referredBy: obj_patient.referredBy,
        additionalNotes: obj_patient.additionalNotes
      };
      return obj;
    };

    patientFactory.loadPatientGrid = function(arg_filters) {
          return rootUrl.all('patient').all('grid').customPOST(arg_filters);
    };

    patientFactory.loadPatientForm = function(id) {
      return rootUrl.one('patient', id).all('form').customGET();
    };

    patientFactory.findById = function(id) {
      return rootUrl.one('patient', id).get();
    };

    patientFactory.findAll = function() {
      return rootUrl.all('patient').getList();
    };

    patientFactory.create = function(obj_patient) {
      /*obj_patient.dateOfBirth = moment(obj_patient.dateOfBirth).format("DD/MM/YYYY");*/
      return rootUrl.all('patient').customPOST(obj_patient);
    };

    patientFactory.update = function(obj_patient) {
      /*obj_patient.dateOfBirth = moment(obj_patient.dateOfBirth).format("DD/MM/YYYY");*/
      return rootUrl.all('patient').customPUT(obj_patient);
    };

    patientFactory.delete = function(obj_patient_ids) {
      return rootUrl.all('patient').all('delete').customPOST(obj_patient_ids);
    };

    return patientFactory;
  }]);
