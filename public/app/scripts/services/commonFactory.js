'use strict'

angular.module('pmsApp').factory('CommonFactory',['Restangular',function(Restangular){

    var commonFactory = {};

    var genderList = [{
        key: 'M',
        value: 'Male'
    }, {
        key: 'F',
        value: 'Female'
    }];

    var maritalList = [{
             key: 'M',
             value: 'Married'
        }, {
             key: 'S',
             value: 'Single'
        }
    ];

    commonFactory.findAllMaritalList = function(){
        return angular.copy(maritalList);
    }

    commonFactory.findMaritalByKey = function(arg_key){
        var result = maritalList.filter(function(element, index, array){
            return(element.key == arg_key);
        });
        return (result.length > 0)? result[0] : {key: '',value: 'Select...'}
    }

    commonFactory.findAllGenderList = function() {
        return angular.copy(genderList);
    };

    commonFactory.findGenderByKey = function(arg_key) {
        var result = genderList.filter(function(element, index, array) {
            return (element.key == arg_key);
        });

        return (result.length > 0) ? result[0] : {key: '', value: 'Select...'};
    };

    return commonFactory;
}]);