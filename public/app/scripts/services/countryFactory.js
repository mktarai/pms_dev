'use strict'

angular.module('pmsApp')
  .factory('CountryFactory', ['Restangular', function(Restangular) {
    var countryFactory = {};

    countryFactory.findAll = function() {
      return Restangular.all('pms').all('country').getList();
    }

    return countryFactory;
  }]);
