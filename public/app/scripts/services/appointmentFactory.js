'use strict'

angular.module('pmsApp')
        .factory('AppointmentFactory',['Restangular', function(Restangular){

        var appointmentFactory = {};

        var rootUrl = Restangular.all('pms');

        appointmentFactory.loadPatientAppointmentForm = function(arg_patient_id) {
                return rootUrl.one('patient', arg_patient_id).all('appointment').customGET();
        };

        appointmentFactory.delete = function(id){
              return rootUrl.one('appointment').customDELETE(id);
        };

        appointmentFactory.create = function(arg_patient_id, obj_appointment){
                return rootUrl.one('patient', arg_patient_id).all('appointment').customPOST(obj_appointment);
        };

        appointmentFactory.update = function(arg_patient_id, obj_appointment){
                return rootUrl.one('patient', arg_patient_id).all('appointment').customPUT(obj_appointment);
        }

        return appointmentFactory;
}]);