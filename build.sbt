name := "pms_dev"

version := "1.0-SNAPSHOT"

libraryDependencies ++= Seq(
  filters,
  cache,
  javaJpa.exclude("org.hibernate.javax.persistence", "hibernate-jpa-2.0-api"),
  "org.eclipse.persistence" % "eclipselink" % "2.5.2",
  "mysql" % "mysql-connector-java" % "5.1.16"
)     

play.Project.playJavaSettings
