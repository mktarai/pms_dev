package model;

import javax.persistence.*;

/**
 * Created by monish on 2/10/2016.
 */
@Table(name = "country")
@Entity
public class Country {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    public Long id;

    @Column(name = "name")
    public String name;

    @Column(name = "nationality")
    public String nationality;

}
