package model;

import javax.persistence.*;
import java.util.Calendar;

/**
 * Created by monish on 2/10/2016.
 */
@Table(name = "patient")
@Entity
public class Patient {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    public Long id;

    @Column(name = "Fname")
    public String firstName;

    @Column(name = "Mname")
    public String middleName;

    @Column(name = "Lname")
    public String lastName;

    @Column(name = "DOB")
    @Temporal(TemporalType.DATE)
    public Calendar dateOfBirth;

    @Column(name = "gender")
    public String gender;

    @Column(name = "marital_status")
    public String maritalStatus;

    @Column(name = "email")
    public String email;

    @Column(name = "address")
    public String address;

    @Column(name = "city")
    public String city;

    @ManyToOne
    @JoinColumn(name = "country")
    public Country country;

    @Column(name = "Cell_Phone")
    public String cellPhone;

    @Column(name = "Alt_Phone")
    public String alternatePhone;

    @Column(name = "Home_Phone")
    public String homePhone;

    @Column(name = "Referred_by")
    public String referredBy;

    @Column(name = "additional_notes")
    public String additionalNotes;
}
