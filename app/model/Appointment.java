package model;

import javax.persistence.*;
import java.util.Calendar;

/**
 * Created by Sangram on 2/16/2016.
 */
@Table(name = "appointment")
@Entity
public class Appointment {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "appointment_id")
    public Long id;

    @ManyToOne
    @JoinColumn(name = "patient_id")
    public Patient patient;

    @Column(name = "appointment_date")
    @Temporal(TemporalType.TIMESTAMP)
    public Calendar appointmentDate;
}
