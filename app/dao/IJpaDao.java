package dao;

import java.util.List;

/**
 * Created by monish on 2/10/2016.
 */
public interface IJpaDao<K, E> {

    public E create(E entity);

    public void delete(E entity);

    public E update(E entity);

    public E findById(K id);

    public List<E> findAll();
}
