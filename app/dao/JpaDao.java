package dao;

import play.db.jpa.JPA;

import javax.persistence.EntityManager;
import java.lang.reflect.ParameterizedType;
import java.util.List;

/**
 * Created by monish on 2/10/2016.
 */
public abstract class JpaDao<K, E> implements IJpaDao<K, E> {

    protected Class<E> entityClass;

    @SuppressWarnings("unchecked")
    public JpaDao() {
        ParameterizedType genericSuperclass = (ParameterizedType) getClass().getGenericSuperclass();
        this.entityClass = (Class<E>) genericSuperclass.getActualTypeArguments()[1];
    }

    protected EntityManager em() {
        return JPA.em();
    }

    public void setRollbackOnly() {
        em().getTransaction().setRollbackOnly();
    }

    public boolean isRollback() {
        return em().getTransaction().getRollbackOnly();
    }

    public E create(E entity) {
        em().persist(entity);
        em().flush();
        return entity;
    }

    public void delete(E entity) {
        em().remove(entity);
        em().flush();
    }

    public E update(E entity) {
        em().merge(entity);
        return entity;
    }

    public E findById(K id) {
        return em().find(entityClass, id);
    }

    public List<E> findAll() {
        return em().createQuery("Select t from " + entityClass.getSimpleName() + " t").getResultList();
    }
}
