package dao;

import model.Appointment;
import play.Logger;

import javax.persistence.NoResultException;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.util.Collections;
import java.util.List;

/**
 * Created by Sangram on 2/17/2016.
 */
public class AppointmentDao extends JpaDao<Long, Appointment> {

    private static final Logger.ALogger logger = Logger.of(AppointmentDao.class);

    private static AppointmentDao appointmentDao = new AppointmentDao();

    public static AppointmentDao getInstance() {
        if (appointmentDao == null) {
            appointmentDao = new AppointmentDao();
        }
        return appointmentDao;
    }

    public void save(Appointment item) {
        try {
            if (item.id == null) {
                create(item);
            } else {
                update(item);
            }
        } catch (Exception e) {
            throw e;
        }
    }

    public void save(List<Appointment> items) {
        for (Appointment item : items) {
            save(item);
        }
    }

    public void remove(Appointment item) {
        try {
            delete(item);
        } catch (Exception e) {
            throw e;
        }
    }

    public void remove(List<Appointment> items) {
        for (Appointment item : items) {
            remove(item);
        }
    }

    /**
     * @param patient_id
     * @return
     */
    public List<Appointment> findByPatientId(long patient_id) {
        CriteriaBuilder criteriaBuilder = em().getCriteriaBuilder();
        CriteriaQuery<Appointment> criteriaQuery = criteriaBuilder.createQuery(Appointment.class);
        Root<Appointment> root = criteriaQuery.from(Appointment.class);
        criteriaQuery.select(root)
                .where(criteriaBuilder.equal(root.get("patient").get("id"), patient_id));

        TypedQuery<Appointment> query = em().createQuery(criteriaQuery);

        try {
            return query.getResultList();
        } catch (NoResultException e) {
            return Collections.emptyList();
        } catch (Exception e) {
            throw e;
        }
    }
}
