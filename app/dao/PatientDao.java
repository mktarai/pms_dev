package dao;

import com.fasterxml.jackson.databind.JsonNode;
import model.Patient;
import play.Logger;

import javax.persistence.NoResultException;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Created by monish on 2/10/2016.
 */
public class PatientDao extends JpaDao<Long, Patient> {

    private static final Logger.ALogger logger = Logger.of(PatientDao.class);

    private static PatientDao patientDao = new PatientDao();

    private PatientDao() {

    }

    public static PatientDao getInstance() {
        if(patientDao == null) {
            patientDao = new PatientDao();
        }
        return patientDao;
    }

    public void save(Patient item) {
        try {
            if(item.id == null) {
                create(item);
            } else {
                update(item);
            }
        } catch (Exception e) {
            throw e;
        }
    }

    public void save(List<Patient> items) {
        for(Patient item: items) {
            save(item);
        }
    }

    public void remove(Patient item) {
        try {
            delete(item);
        } catch (Exception e) {
            throw e;
        }
    }

    public void remove(List<Patient> items) {
        for(Patient item : items) {
            remove(item);
        }
    }

    public List<Patient> findByFilterAndGridParam(JsonNode filter_param_node) {
        CriteriaBuilder criteriaBuilder = em().getCriteriaBuilder();
        CriteriaQuery<Patient> criteriaQuery = criteriaBuilder.createQuery(Patient.class);
        Root<Patient> root = criteriaQuery.from(Patient.class);
        criteriaQuery.select(root);

        List<Predicate> whereClause = new ArrayList<>();
        if(filter_param_node.get("name").asText().length() > 0) {
            Predicate p1 = criteriaBuilder.like(root.<String>get("firstName"), "%" + filter_param_node.get("name").asText() + "%");
            Predicate p2 = criteriaBuilder.like(root.<String>get("middleName"), "%" + filter_param_node.get("name").asText() + "%");
            Predicate p3 = criteriaBuilder.like(root.<String>get("lastName"), "%" + filter_param_node.get("name").asText() + "%");
            whereClause.add(criteriaBuilder.or(p1, p2, p3));
        }

        if(filter_param_node.get("email").asText().length() > 0) {
            whereClause.add(criteriaBuilder.equal(root.get("email"), filter_param_node.get("email").asText()));
        }

        if(filter_param_node.get("contact").asText().length() > 0){
            Predicate cellPhone = criteriaBuilder.equal(root.get("cellPhone"), filter_param_node.get("cellPhone").asText());
            Predicate alternatePhone = criteriaBuilder.equal(root.get("alternatePhone"), filter_param_node.get("alternatePhone").asText());
            Predicate homePhone = criteriaBuilder.equal(root.get("homePhone"), filter_param_node.get("homePhone").asText());
            whereClause.add(criteriaBuilder.or(cellPhone,alternatePhone,homePhone));
        }

        if(filter_param_node.get("referredBy").asText().length() > 0){
            whereClause.add(criteriaBuilder.like(root.<String>get("referredBy"), "%" + filter_param_node.get("referredBy").asText() + "%"));
        }

        if(filter_param_node.get("matchAll").asBoolean()) {
            criteriaQuery.where(criteriaBuilder.and(whereClause.toArray(new Predicate[]{})));
        } else {
            criteriaQuery.where(criteriaBuilder.or(whereClause.toArray(new Predicate[]{})));
        }

        TypedQuery<Patient> typedQuery = em().createQuery(criteriaQuery);

        try {
            return typedQuery.getResultList();
        } catch (NoResultException e) {
            return Collections.emptyList();
        } catch (Exception e) {
            throw e;
        }
    }

    public List<Patient> findByIdsIn(List<Long> patient_ids) {
        CriteriaBuilder criteriaBuilder = em().getCriteriaBuilder();
        CriteriaQuery<Patient> criteriaQuery = criteriaBuilder.createQuery(Patient.class);
        Root<Patient> root = criteriaQuery.from(Patient.class);
        criteriaQuery.select(root)
                .where(root.get("id").in(patient_ids));

        TypedQuery<Patient> query = em().createQuery(criteriaQuery);

        try {
            return query.getResultList();
        } catch (NoResultException e) {
            return Collections.emptyList();
        } catch (Exception e) {
            throw e;
        }
    }
}
