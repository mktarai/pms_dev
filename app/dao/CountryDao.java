package dao;

import model.Country;

import java.util.List;

/**
 * Created by monish on 2/10/2016.
 */
public class CountryDao extends JpaDao<Long, Country> {

    private static CountryDao countryDao = new CountryDao();

    private CountryDao() {

    }

    public static CountryDao getInstance() {
        if(countryDao == null) {
            countryDao = new CountryDao();
        }
        return countryDao;
    }

    public void save(Country item) {
        try {
            if(item.id == null) {
                create(item);
            } else {
                update(item);
            }
        } catch (Exception e) {
            throw e;
        }
    }

    public void save(List<Country> items) {
        for(Country item: items) {
            save(item);
        }
    }

    public void remove(Country item) {
        try {
            delete(item);
        } catch (Exception e) {
            throw e;
        }
    }

    public void remove(List<Country> items) {
        for(Country item : items) {
            remove(item);
        }
    }
}
