package controllers;

import bo.CountryBo;
import bo.PatientBo;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import model.Patient;
import play.Logger;
import play.db.jpa.Transactional;
import play.libs.Json;
import play.mvc.Controller;
import play.mvc.Http;
import play.mvc.Result;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by monish on 2/10/2016.
 */
public class PatientController extends Controller {

    private static final Logger.ALogger logger = Logger.of(PatientController.class);

    private PatientBo patientBo = PatientBo.getInstance();
    private CountryBo countryBo = CountryBo.getInstance();

    @Transactional
    public Result findById(Long id) {
        Result r = internalServerError();

        try {
            ObjectMapper mapper = new ObjectMapper();
            Patient p = patientBo.getPatientDetails(id);
            if(p != null) {
                r = ok(mapper.writeValueAsString(p));
            } else {
                r = notFound();
            }
        } catch (Exception e) {
            logger.error("Error:", e);
        }
        return r;
    }

    @Transactional
    public Result loadPatientGrid(){
        Result r = internalServerError();

        try {
            JsonNode criteriaNode = request().body().asJson();
            List<Patient> response = patientBo.loadPatientGrid(criteriaNode);

            r = ok(Json.toJson(response));
        } catch (Exception e) {
            logger.error("Error:", e);
        }
        return r;
    }

    @Transactional
    public Result findAll() {
        Result r = internalServerError();

        try {
            ObjectMapper mapper = new ObjectMapper();
            List<Patient> patientList = patientBo.getAllPatients();
            r = ok(mapper.writeValueAsString(patientList));
        } catch (Exception e) {
            logger.error("Error:", e);
        }
        return r;
    }

    @Transactional
    public Result loadPatientForm(Long id) {
        Result r = internalServerError();

        try {
            ObjectMapper mapper = new ObjectMapper();
            ObjectNode response = mapper.createObjectNode();
            Patient p = patientBo.getPatientDetails(id);
            if(p != null) {
                response.put("patientInfo", mapper.writeValueAsString(patientBo.getPatientDetails(id)));
                response.put("countryList", mapper.writeValueAsString(countryBo.getAllCountries()));
                r = ok(response);
            } else {
                r = notFound(response.put("message", "Patient not found."));
            }
        } catch (Exception e) {
            logger.error("Error:", e);
        }
        return r;
    }

    private Result addUpdate(Http.Request request) {
        Result r = internalServerError();

        try {
            JsonNode jsonNodePatient = request.body().asJson();
            ObjectNode response = patientBo.addUpdate(jsonNodePatient);

            if(response.get("message").asText().contentEquals(PatientBo.ADD_SUCCESS) || response.get("message").asText().contentEquals(PatientBo.UPDATE_SUCCESS)) {
                r = ok(response);
            } else {
                r = internalServerError(response);
            }
        } catch (Exception e) {
            logger.error("Error:", e);
        }
        return r;
    }

    @Transactional
    public Result create() {
        return addUpdate(request());
    }

    @Transactional
    public Result update() {
        return addUpdate(request());
    }

    @Transactional
    public Result delete() {
        Result r = internalServerError();

        try {
            ArrayNode arrayNode = (ArrayNode)request().body().asJson();

            if(arrayNode.size() > 0) {
                List<Long> patientIds = new ArrayList<>();
                for(JsonNode node : arrayNode) {
                    patientIds.add(node.asLong());
                }

                ObjectNode response = patientBo.delete(patientIds);

                if(response.get("message").asText().contentEquals(PatientBo.DELETE_SUCCESS)) {
                    r = ok(response);
                } else if(response.get("message").asText().contentEquals(PatientBo.NONE_TO_DELETE)) {
                    r = badRequest(response);
                } else {
                    r = internalServerError(response);
                }
            } else {
                r = badRequest();
            }
        } catch (Exception e) {
            logger.error("Error:", e);
        }
        return r;
    }
}
