package controllers;

import bo.AppointmentBo;
import bo.PatientBo;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import model.Appointment;
import model.Patient;
import play.Logger;
import play.db.jpa.Transactional;
import play.libs.Json;
import play.mvc.Controller;
import play.mvc.Http;
import play.mvc.Result;

import java.util.List;

/**
 * Created by Sangram on 2/17/2016.
 */
public class AppointmentController extends Controller {

    private static final Logger.ALogger logger = Logger.of(AppointmentController.class);

    private PatientBo patientBo = PatientBo.getInstance();
    private AppointmentBo appointmentBo = AppointmentBo.getInstance();

    @Transactional
    public Result delete(Long id) {
        Result r = internalServerError();

        ObjectNode response = appointmentBo.delete(id);
        if (response.get("message").asText().contentEquals(AppointmentBo.DELETE_SUCCESS)) {
            r = ok(response);
        } else if (response.get("message").asText().contentEquals(AppointmentBo.FAILURE)) {
            r = badRequest(response);
        }

        return r;
    }

    @Transactional
    public Result loadPatientAppointmentForm(Long id) {
        Result r = internalServerError();

        try {
            ObjectMapper mapper = new ObjectMapper();
            ObjectNode response = mapper.createObjectNode();
            Patient patient = patientBo.getPatientDetails(id);
            if(patient != null) {
                List<Appointment> appointmentList = appointmentBo.getAllPatientAppointmentHistory(id);
                if(appointmentList.size() <= 0) {
                    response.put("patient", Json.toJson(patient));
                }
                response.put("appointmentList", Json.toJson(appointmentList));
                r = ok(response);
            } else {
                r = notFound(response.put("message", "Patient not found."));
            }
        } catch (Exception e) {
            logger.error("Error :", e);
        }

        return r;
    }

    private Result addUpdate(Http.Request request, Long patient_id) {
        Result r = internalServerError();

        try {
            JsonNode jsonNodeAppointment = request.body().asJson();
            ObjectNode response = appointmentBo.addUpdate(patient_id, jsonNodeAppointment);

            if (response.get("message").asText().contentEquals(AppointmentBo.ADD_SUCCESS) || response.get("message").asText().contentEquals(AppointmentBo.UPDATE_SUCCESS)) {
                return ok(response);
            } else {
                return internalServerError();
            }
        } catch (Exception e) {
            logger.error("Error: ", e);
        }
        return r;
    }

    @Transactional
    public Result create(Long id) {
        return addUpdate(request(), id);
    }

    @Transactional
    public Result update(Long id) {
        return addUpdate(request(), id);
    }
}
