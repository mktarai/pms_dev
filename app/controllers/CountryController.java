package controllers;

import bo.CountryBo;
import com.fasterxml.jackson.databind.ObjectMapper;
import model.Country;
import play.db.jpa.Transactional;
import play.mvc.Controller;
import play.mvc.Result;

import java.util.List;

/**
 * Created by monish on 2/11/2016.
 */
public class CountryController extends Controller {

    private CountryBo countryBo = CountryBo.getInstance();

    @Transactional
    public Result findAll() {
        Result r = internalServerError();

        try {
            ObjectMapper mapper = new ObjectMapper();
            List<Country> countryList = countryBo.getAllCountries();
            r = ok(mapper.writeValueAsString(countryList));
        } catch (Exception e) {

        }

        return r;
    }
}
