package bo;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import dao.AppointmentDao;
import dao.PatientDao;
import model.Appointment;
import play.Logger;
import play.libs.Json;
import util.DateUtil;

import java.util.List;

/**
 * Created by Sangram on 2/17/2016.
 */
public class AppointmentBo {

    private static final Logger.ALogger logger = Logger.of(AppointmentBo.class);

    public static final String ADD_SUCCESS = "The Appointment was scheduled successfully.";
    public static final String UPDATE_SUCCESS = "The Appointment was updated successfully.";
    public static final String DELETE_SUCCESS = "The Appointment was deleted successfully.";
    public static final String SUCCESS = "Success";
    public static final String FAILURE = "Failure";

    private static AppointmentBo appointmentBo = new AppointmentBo();

    private AppointmentDao appointmentDao = AppointmentDao.getInstance();
    private PatientDao patientDao = PatientDao.getInstance();

    private AppointmentBo() {

    }

    public static AppointmentBo getInstance() {
        if (appointmentBo == null) {
            appointmentBo = new AppointmentBo();
        }
        return appointmentBo;
    }

    /**
     * @param json_node_appointment
     * @return
     */
    public ObjectNode addUpdate(Long patient_id, JsonNode json_node_appointment) {
        ObjectMapper mapper = new ObjectMapper();
        ObjectNode response = mapper.createObjectNode();
        response.put("message", FAILURE);

        if (json_node_appointment == null && json_node_appointment.get("bookDate").asText().length() <= 0) {
            return response;
        }

        try {
            String mode;
            Appointment appointment;
            if (json_node_appointment.get("id").asText().length() <= 0) {
                appointment = new Appointment();
                mode = ADD_SUCCESS;
            } else {
                appointment = appointmentDao.findById(json_node_appointment.get("id").asLong());
                mode = UPDATE_SUCCESS;
            }

            appointment.patient = patientDao.findById(patient_id);
            String bookDateTimeStr = json_node_appointment.get("bookDate").asText() + " " + json_node_appointment.get("bookTime").asText();
            appointment.appointmentDate = DateUtil.GetDateFromString(bookDateTimeStr, "dd/MM/yyyy hh:mm a");

            appointmentDao.save(appointment);

            response.put("appointment", Json.toJson(appointment));
            response.put("message", mode);
        } catch (Exception e) {
            logger.error("Error", e);
            response.put("message", FAILURE);
        }
        return response;
    }

    public List<Appointment> getAllPatientAppointmentHistory(Long patient_id) {
        return appointmentDao.findByPatientId(patient_id);
    }

    /**
     * @param appointment_id
     * @return
     */
    public ObjectNode delete(Long appointment_id) {

        ObjectMapper mapper = new ObjectMapper();
        ObjectNode response = mapper.createObjectNode();

        response.put("message", FAILURE);
        try {
            Appointment appointment = appointmentDao.findById(appointment_id);
            appointmentDao.remove(appointment);
            response.put("message", DELETE_SUCCESS);
        } catch (Exception e) {
            logger.error("Error :", e);
        }

        return response;
    }
}
