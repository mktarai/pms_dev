package bo;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import dao.CountryDao;
import dao.PatientDao;
import model.Patient;
import play.Logger;
import util.DateUtil;

import java.util.List;

/**
 * Created by monish on 2/10/2016.
 */
public class PatientBo {

    private static final Logger.ALogger logger = Logger.of(PatientBo.class);

    public static final String ADD_SUCCESS = "The record was added successfully.";
    public static final String UPDATE_SUCCESS = "The record was updated successfully.";
    public static final String DELETE_SUCCESS = "The record was deleted successfully.";
    public static final String NO_ID_SELECTED = "No Patient Id(s) has/have been selected.";
    public static final String NONE_TO_DELETE = "No Patients found to delete.";
    public static final String SUCCESS = "Success";
    public static final String FAILURE = "Failure";

    private static PatientBo patientBo = new PatientBo();

    private PatientDao patientDao = PatientDao.getInstance();
    private CountryDao countryDao = CountryDao.getInstance();

    private PatientBo() {

    }

    public static PatientBo getInstance() {
        if (patientBo == null) {
            patientBo = new PatientBo();
        }
        return patientBo;
    }

    public Patient getPatientDetails(Long id) {
        return patientDao.findById(id);
    }

    public List<Patient> getAllPatients() {
        return patientDao.findAll();
    }

    public  List<Patient> loadPatientGrid(JsonNode filter_param_node){
        return patientDao.findByFilterAndGridParam(filter_param_node);
    }


    /**
     * @param json_node_patient
     * @return
     */
    public ObjectNode addUpdate(JsonNode json_node_patient) {
        ObjectMapper mapper = new ObjectMapper();
        ObjectNode response = mapper.createObjectNode();
        response.put("message", FAILURE);

        if (json_node_patient == null || (json_node_patient.get("firstName") != null && json_node_patient.get("firstName").asText().length() <= 0)) {
            return response;
        }

        try {
            String mode;
            Patient p;
            if (json_node_patient.get("id").asText().length() <= 0) {
                p = new Patient();
                mode = ADD_SUCCESS;
            } else {
                p = patientDao.findById(json_node_patient.get("id").asLong());
                mode = UPDATE_SUCCESS;
            }

            p.additionalNotes = json_node_patient.get("additionalNotes").asText();
            p.address = json_node_patient.get("address").asText();
            p.alternatePhone = json_node_patient.get("alternatePhone").asText();
            p.cellPhone = json_node_patient.get("cellPhone").asText();
            p.city = json_node_patient.get("city").asText();
            p.country = countryDao.findById(json_node_patient.get("country").get("id").asLong());
            p.dateOfBirth = DateUtil.GetDateFromString(json_node_patient.get("dateOfBirth").asText());
            p.email = json_node_patient.get("email").asText();
            p.firstName = json_node_patient.get("firstName").asText();
            p.gender = json_node_patient.get("gender").get("key").asText();
            p.homePhone = json_node_patient.get("homePhone").asText();
            p.lastName = json_node_patient.get("lastName").asText();
            p.maritalStatus = json_node_patient.get("maritalStatus").get("key").asText();
            p.middleName = json_node_patient.get("middleName").asText();
            p.referredBy = json_node_patient.get("referredBy").asText();

            patientDao.save(p);

            response.put("id", p.id);
            response.put("message", mode);
        } catch (Exception e) {
            logger.error("Error:", e);
            response.put("message", FAILURE);
        }

        return response;
    }

    /**
     * @param patient_ids
     * @return
     */
    public ObjectNode delete(List<Long> patient_ids) {
        ObjectMapper mapper = new ObjectMapper();
        ObjectNode response = mapper.createObjectNode();
        response.put("message", FAILURE);

        try {
            List<Patient> patientList = patientDao.findByIdsIn(patient_ids);

            if(patientList.size() > 0) {
                patientDao.remove(patientList);
                response.put("message", DELETE_SUCCESS);
            } else {
                response.put("message", NONE_TO_DELETE);
            }
        } catch (Exception e) {
            logger.error("Error:", e);
            response.put("message", FAILURE);
        }

        return response;
    }
}
