package bo;

import dao.CountryDao;
import model.Country;

import java.util.List;

/**
 * Created by monish on 2/11/2016.
 */
public class CountryBo {

    private static CountryBo countryBo = new CountryBo();

    private CountryDao countryDao = CountryDao.getInstance();

    private CountryBo() {

    }

    public static CountryBo getInstance() {
        if(countryBo == null) {
            countryBo = new CountryBo();
        }
        return countryBo;
    }

    public List<Country> getAllCountries() {
        return countryDao.findAll();
    }
}
