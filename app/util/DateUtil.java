package util;

import java.text.SimpleDateFormat;
import java.util.Calendar;

/**
 * Created by monish on 2/11/2016.
 */
public final class DateUtil {

    private static SimpleDateFormat sdf1 = new SimpleDateFormat("dd/MM/yyyy");

    private DateUtil() {
        // prevent instantiation
    }

    public static Calendar GetDateFromString(String arg_string_date) {
        return GetDateFromString(arg_string_date, null);
    }

    public static Calendar GetDateFromString(String arg_string_date, String arg_date_format) {
        try {
            Calendar cal = Calendar.getInstance();
            if(arg_date_format != null) {
                SimpleDateFormat sdf2 = new SimpleDateFormat(arg_date_format);
                cal.setTime(sdf2.parse(arg_string_date));
            } else {
                cal.setTime(sdf1.parse(arg_string_date));
            }

            return cal;
        } catch (Exception e) {
            return null;
        }
    }

    public static String GetFormattedDate(Calendar arg_date) {
        return GetFormattedDate(arg_date, null);
    }

    public static String GetFormattedDate(Calendar arg_date, String arg_format) {
        try {
            if(arg_format == null || arg_format.length() <= 0) {
                return sdf1.format(arg_date.getTime());
            } else {
                SimpleDateFormat sdf2 = new SimpleDateFormat(arg_format);
                return sdf2.format(arg_date.getTime());
            }
        } catch (Exception e) {
            return "";
        }
    }
}
